package facci.pm.merovelez.bd;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class IngresoActivity extends AppCompatActivity {
    EditText siglas, nombreUniversidad, carrera, categoria, ciudad;
    Button btnGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingreso);

        siglas = findViewById(R.id.editTextSigla);
        nombreUniversidad = findViewById(R.id.editTextNombreUniveridad);
        carrera = findViewById(R.id.editTextCarrera);
        categoria = findViewById(R.id.editTextCategoria);
        ciudad = findViewById(R.id.editTextCiudad);

        btnGuardar = findViewById(R.id.btnGuardar);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!siglas.getText().toString().trim().isEmpty() && !nombreUniversidad.getText().toString().trim().isEmpty() &&
                        !carrera.getText().toString().trim().isEmpty() && !categoria.getText().toString().trim().isEmpty() &&
                        !ciudad.getText().toString().trim().isEmpty()) {

                    Universidad universidadGuardar = new Universidad(siglas.getText().toString(), nombreUniversidad.getText().toString(),
                            carrera.getText().toString(),
                            categoria.getText().toString(), ciudad.getText().toString());
                    universidadGuardar.save();
                    finish();

                    Toast toast1 = Toast.makeText(getApplicationContext(), "Se guardo el dato", Toast.LENGTH_SHORT);
                    toast1.show();

                }

            }
        });
    }
}
