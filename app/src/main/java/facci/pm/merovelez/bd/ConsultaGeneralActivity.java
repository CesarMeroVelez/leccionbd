package facci.pm.merovelez.bd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ConsultaGeneralActivity extends AppCompatActivity {
    ListView consultaListaGeneral;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta_general);

        consultaListaGeneral = findViewById(R.id.listaConsultaGeneral);

        final List<Universidad> listaUniversidad = Universidad.listAll(Universidad.class);
        List<String> listaUniversidad2 = new ArrayList<>();

        for (Universidad i: listaUniversidad) {
            listaUniversidad2.add(i.getSIGLAS() + "\n" +
                    i.getNombreUniversidad() + "\n"+
                    i.getCarrera() + "\n" +
                    i.getCategoria()+ "\n"+
                    i.getCiudad());


        }

        ArrayAdapter<String> adaptador = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, listaUniversidad2);
        consultaListaGeneral.setAdapter(adaptador);

        //MODIFICAR

        consultaListaGeneral.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Universidad telefonoModificar = listaUniversidad.get(position);
                Intent intent = new Intent(ConsultaGeneralActivity.this, ModificarActivity.class);
                Bundle bundle = new Bundle();
                bundle.putLong("id", telefonoModificar.getId());
                bundle.putString("sigla", telefonoModificar.getSIGLAS());
                bundle.putString("nombreUniversidad", telefonoModificar.getNombreUniversidad());
                bundle.putString("carrera", telefonoModificar.getCarrera());
                bundle.putString("categoria", telefonoModificar.getCategoria());
                bundle.putString("ciudad", telefonoModificar.getCiudad());
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });


        //ELIMINAR
        consultaListaGeneral.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Universidad universidadEliminar = listaUniversidad.get(position);
                universidadEliminar.delete();
                refrescarLista();
                Toast toast1 = Toast.makeText(getApplicationContext(), "El dato se eliminó con éxito", Toast.LENGTH_SHORT);
                toast1.show();
                return true;

            }

        });


    }

    private void refrescarLista(){
        final List<Universidad> listaUniversidad = Universidad.listAll(Universidad.class);
        List<String> listaUniversidad2 = new ArrayList<>();

        for (Universidad i: listaUniversidad) {
            listaUniversidad2.add(i.getSIGLAS() + "\n" +
                    i.getNombreUniversidad() + "\n"+
                    i.getCarrera() + "\n" +
                    i.getCategoria()+ "\n"+
                    i.getCiudad());


        }

        ArrayAdapter<String> adaptador = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, listaUniversidad2);
        consultaListaGeneral.setAdapter(adaptador);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        refrescarLista();
    }
}
