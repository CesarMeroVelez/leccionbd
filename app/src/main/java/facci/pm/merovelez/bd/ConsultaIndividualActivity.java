package facci.pm.merovelez.bd;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class ConsultaIndividualActivity extends AppCompatActivity {
    EditText buscarSigla;
    Button btnBuscarSigla;
    TextView textoConsulta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta_individual);

        buscarSigla = findViewById(R.id.editTextBuscarSigla);
        btnBuscarSigla = findViewById(R.id.btnBuscarSigla);
        textoConsulta = findViewById(R.id.textViewDato);

        btnBuscarSigla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!buscarSigla.getText().toString().trim().isEmpty()){
                    List<Universidad> universidadBuscarSigla = Universidad.find(Universidad.class, "SIGLAS = ?",buscarSigla.getText().toString());

                    if(universidadBuscarSigla.size() > 0){

                        Universidad universidadEncontrada = universidadBuscarSigla.get(0);
                        textoConsulta.setText(universidadEncontrada.getSIGLAS() + "\n" +
                                universidadEncontrada.getNombreUniversidad() + "\n"+
                                universidadEncontrada.getCarrera() + "\n"+
                                universidadEncontrada.getCategoria() + "\n"+
                                universidadEncontrada.getCiudad());
                        Toast toast1 = Toast.makeText(getApplicationContext(), "Resultado encontrado", Toast.LENGTH_SHORT);
                        toast1.show();

                    }else {
                        textoConsulta.setText("No se encontró ninguna Universidad");
                    }


                }

            }
        });


    }
}
