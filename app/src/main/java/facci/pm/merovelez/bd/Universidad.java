package facci.pm.merovelez.bd;

import com.orm.SugarRecord;

public class Universidad extends SugarRecord<Universidad> {

    private String SIGLAS;
    private String nombreUniversidad;
    private String carrera;
    private String categoria;
    private String ciudad;

    public Universidad() {
    }

    public Universidad(String SIGLAS, String nombreUniversidad, String carrera, String categoria, String ciudad) {
        this.SIGLAS = SIGLAS;
        this.nombreUniversidad = nombreUniversidad;
        this.carrera = carrera;
        this.categoria = categoria;
        this.ciudad = ciudad;
    }

    public String getSIGLAS() {
        return SIGLAS;
    }

    public void setSIGLAS(String SIGLAS) {
        this.SIGLAS = SIGLAS;
    }

    public String getNombreUniversidad() {
        return nombreUniversidad;
    }

    public void setNombreUniversidad(String nombreUniversidad) {
        this.nombreUniversidad = nombreUniversidad;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
}
