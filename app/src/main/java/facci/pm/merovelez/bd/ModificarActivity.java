package facci.pm.merovelez.bd;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ModificarActivity extends AppCompatActivity {

    EditText siglas, nombreUniversidad, carrera, categoria, ciudad;
    Button btnGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar);

        siglas = findViewById(R.id.editTextSigla_modificar);
        nombreUniversidad = findViewById(R.id.editTextNombreUniveridad_modificar);
        carrera = findViewById(R.id.editTextCarrera_modificar);
        categoria = findViewById(R.id.editTextCategoria_modificar);
        ciudad = findViewById(R.id.editTextCiudad_modificar);

        btnGuardar = findViewById(R.id.btnGuardar_modificar);

        final Bundle bundle = this.getIntent().getExtras();

        siglas.setText(bundle.getString("sigla"));
        nombreUniversidad.setText(bundle.getString("nombreUniversidad"));
        carrera.setText(bundle.getString("carrera"));
        categoria.setText(bundle.getString("categoria"));
        ciudad.setText(bundle.getString("ciudad"));

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!siglas.getText().toString().trim().isEmpty() && !nombreUniversidad.getText().toString().trim().isEmpty() &&
                        !carrera.getText().toString().trim().isEmpty() && !categoria.getText().toString().trim().isEmpty() &&
                        !ciudad.getText().toString().trim().isEmpty()) {

                    Universidad universidadModificar = Universidad.findById(Universidad.class, bundle.getLong("id"));
                    universidadModificar.setSIGLAS(siglas.getText().toString());
                    universidadModificar.setNombreUniversidad(nombreUniversidad.getText().toString());
                    universidadModificar.setCarrera(carrera.getText().toString());
                    universidadModificar.setCategoria(categoria.getText().toString());
                    universidadModificar.setCiudad(ciudad.getText().toString());
                    universidadModificar.save();
                    finish();
                    Toast toast1 = Toast.makeText(getApplicationContext(), "Datos modificados con exito", Toast.LENGTH_SHORT);
                    toast1.show();


                }
            }
        });

    }
}
